// Converts C style string to NSString
NSString* CreateNSString1 (const char* string)
{
	if (string)
		return [NSString stringWithUTF8String: string];
	else
		return [NSString stringWithUTF8String: ""];
}

// When native code plugin is implemented in .mm / .cpp file, then functions
// should be surrounded with extern "C" block to conform C function naming rules
extern "C" {

	void _SaveImageToAlbumAtPath (const char* path)
	{
        UIImage *image = [UIImage imageNamed:@"target.png"];
        
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ARDrift" message:@"Сохранено в альбом" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
		[alert show];
		[alert release];
    }

}

