//
//  UnityPlugin.h
//  unityPlugin
//
//  Created by Nap1 on 25.02.13.
//  Copyright (c) 2013 Napoleon IT. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "InAppPurchasePlugin.h"
#import "MBProgressHUD.h"

@interface UnityPlugin : NSObject {
    UIView *hudAddedTo;
}

//UI blocking progress HUD
@property (retain) MBProgressHUD *hud;

@end
