public var customSkin : GUISkin;
public var playGameRect : Rect;

function Start()
{
	if(SystemInfo.deviceModel=="iPhone")
	{
		playGameRect.height=60;
		playGameRect.width=60;
	}
	else
	{
		playGameRect.height=98;
		playGameRect.width=98;
	}
}
function OnGUI()
{
	GUI.skin = customSkin;
	
	if(GUI.Button(playGameRect," "))
	{
		if(Application.loadedLevelName=="QCAR-Free")
		{
	    	Application.LoadLevel("QCAR-Free");
		}
		if(Application.loadedLevelName=="QCAR-ImageTargets")
		{
	    	Application.LoadLevel("QCAR-ImageTargets");
		}
	}
}