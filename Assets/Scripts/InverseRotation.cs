using UnityEngine;
using System.Collections;

public class InverseRotation : MonoBehaviour 
{
	MainMenu other;
	private GUITexture gui;
	void Awake () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		gui = (GUITexture)GetComponent(typeof(GUITexture));
		if(other.inverse)
		{
			transform.position = new Vector3(0.8f,0.1f,0);
			this.transform.localScale = new Vector3(0.12f,0.16f,1);
			gui.color = Color.gray;
		}
		else
		{
			this.transform.position = new Vector3(0.1f, 0.1f, 0);
			this.transform.localScale = new Vector3(0.12f,0.16f,1);
			gui.color = Color.gray;
		}
	}
}
