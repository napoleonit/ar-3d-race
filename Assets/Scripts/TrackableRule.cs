using UnityEngine;
using System.Collections;

public class TrackableRule : MonoBehaviour 
{
	
	DefaultTrackableEventHandler other;
	// Use this for initialization
	void Start () 
	{
		other = GameObject.Find("ImageTargetStones").GetComponent("DefaultTrackableEventHandler") as DefaultTrackableEventHandler;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(other.statusTracked)
		{
			this.gameObject.transform.localScale = new Vector3(0.07f, 0.1f, 0.07f);
		}
		else
		{
			this.gameObject.transform.localScale = new Vector3(0, 0, 0);
		}
	}
}
