using UnityEngine;
using System.Collections;
using System.IO;

public class Instructions : MonoBehaviour 
{
	string path;
	bool show = false;
	public Texture htp;
	public Texture htp2;
	Rect htpRect;
	Rect saveRect;
	Rect startRect;
	int scaleGUI;
	public GUISkin clearSkin;
	
	// Use this for initialization
	void Start () 
	{
		path = GetPath() + "/Instructions.dat";
		scaleGUI = Screen.width/1024;
		htpRect = new Rect(0*scaleGUI,-4*scaleGUI,1030*scaleGUI,774*scaleGUI);
		saveRect = new Rect(131*scaleGUI,71*scaleGUI,66*scaleGUI,39*scaleGUI);
		startRect = new Rect(472*scaleGUI,669*scaleGUI,107*scaleGUI,60*scaleGUI);
		
		if (!File.Exists(path))
		{
			show = true;
        	using (StreamWriter sw = File.CreateText(path))
        	{
        	    sw.WriteLine("0");
				sw.Close();
        	}
		}
	}
	
	void OnGUI()
	{
		if(show)
		{
			int saveDepth = GUI.depth;
			GUI.depth = 1;
			GUI.Label(htpRect, scaleGUI==2? htp : htp2);
			
			GUI.skin = clearSkin;
			if(GUI.Button(saveRect, ""))
			{
				
			}
			if(GUI.Button(startRect, ""))
			{
				show = false;
			}
			GUI.depth = saveDepth;
		}
	}
	
	string GetPath()
	{
		string root = Application.dataPath.Substring(0, Application.dataPath.Length - 26) + "/Documents";
		#if UNITY_EDITOR
		root = Application.dataPath.Substring(0, Application.dataPath.Length);
		#endif
        return root;
	}
}
