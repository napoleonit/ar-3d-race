/*
 * устанавливает ткстуры у джойстиков
 */ 
using UnityEngine;
using System.Collections;

public class SetJoyTexture : MonoBehaviour 
{
	MainMenu other;
	public Joystick touchPad;
	public Texture2D normalTexture;
	public Texture2D clickTexture;
	public Texture2D normalTexture2;
	public Texture2D clickTexture2;

	void Start () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
	}
	
	void Update() 
	{
		if(touchPad.position.y>0)
		{
			this.guiTexture.texture = other.scaleGUI==2? clickTexture : clickTexture2;
		}
		else this.guiTexture.texture = other.scaleGUI==2? normalTexture : normalTexture2;
	}
}
