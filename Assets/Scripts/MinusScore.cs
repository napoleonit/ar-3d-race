/*
 * записывает в файл сколько очков нужно отнять за сбитые фишки и заборы 
 */
using UnityEngine;
using System.Collections;
using System.IO;

public class MinusScore : MonoBehaviour 
{
	public static int minusScore;
	FinishSecondScene other;
	string path;
	void Start () 
	{
		path=GetPath()+"/ScoreMinus.txt";
		minusScore=0;
		other = GameObject.Find("Finish").GetComponent("FinishSecondScene") as FinishSecondScene;
	}
	
	void Update () 
	{
		if(other.go)
		{
			using (StreamWriter sw = File.CreateText(path))
        	{
        	    sw.WriteLine(minusScore);
				sw.Close();
        	}
		}
	}
	
	void OnCollisionEnter(Collision collision) 
	{
		if(collision.gameObject.name=="roadblock low1" || collision.gameObject.name=="Fence on road N060712" || collision.gameObject.name=="zab low pol" || collision.gameObject.name=="zab low pol ext" || collision.gameObject.name=="Rectangle001")
		{
			minusScore+=5;
			other.rammedCount++;
		}
    }
	string GetPath()
	{
		string root = Application.dataPath.Substring(0, Application.dataPath.Length - 26) + "/Documents";
		#if UNITY_EDITOR
		root = Application.dataPath.Substring(0, Application.dataPath.Length);
		#endif
        return root;
	}
}
