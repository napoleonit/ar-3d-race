/*
 * скрипт загрузки бандла выбора машин 
 */
using UnityEngine;
using System.Collections;

public class CarChoiseLoader : MonoBehaviour 
{
	public GameObject go;
	public static AssetBundle bundle;
	static int count=1;
	IEnumerator Start () 
	{
		string url ="file://"+Application.dataPath.Substring(0, Application.dataPath.Length - 4) + "QCAR/ChoiseCar.unity3d";
    	WWW www = WWW.LoadFromCacheOrDownload (url, count++);
    	yield return www;
    	bundle = www.assetBundle;
    	go = bundle.Load("ChoiseCar", typeof(GameObject)) as GameObject;
    	Instantiate(go);
	}
}
