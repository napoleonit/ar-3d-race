/*
 * скрипт ленивой загрузки выбора трасс
 */ 
using UnityEngine;
using System.Collections;

public class TrackChoise : MonoBehaviour 
{
	Rect nextTrackRect;
	Rect lastTrackRect;
	Rect wingsMiniRect;
	Rect siteRect;
	Rect firstMapNameRect;	
	Rect firstMap;
	Rect lockerRect;
	
	public GUISkin nextTrackSkin;
	public GUISkin lastTrackSkin;
	public GUISkin siteSkin;
	public GUISkin wingsMiniSkin;
	public GUISkin firstMapNameSkin;
	public GUISkin backSkin;
	public GUISkin PlayGameSkin;
	public GUISkin lockerSkin;
	public GUISkin playDisSkin;

	public Texture2D firstMapTexture;
	public Texture2D secondMapTexture;
	public Texture2D thirdMapTexture;
	public Texture2D fourthMapTexture;
	public Texture2D fifthMapTexture;
	public Texture2D sixthMapTexture;
	public Texture2D seventhMapTexture;
	public Texture2D eighthMapTexture;
	public Texture2D ninethMapTexture;
	public Texture2D tenthMapTexture;
	public Texture2D eleventhMapTexture;
	public Texture2D twelvethMapTexture;
	public Texture2D threeteenthMapTexture;
	public Texture2D fourteenthMapTexture;
	public Texture2D fifteenthMapTexture;
	public Texture2D sixteenthMapTexture;
	public Texture2D seventeenthMapTexture;
	public Texture2D eighteenthMapTexture;
	public Texture2D nineteenthMapTexture;
	public Texture2D twentyMapTexture;
	public Texture2D twentyoneMapTexture;
	public Texture2D twentytwoMapTexture;
	public Texture2D twentythreeMapTexture;
	public Texture2D twentyfourMapTexture;
	public Texture2D twentyfiveMapTexture;
	
	const int LAST_MAP_CONST = 25*2-1;
	const int FIRST_MAP_CONST = 1;
	const int NORMAL_MAP_CONST = 5;
	const int MINI_MAP_CONST = 6;
	int scaleGUI;
	MainMenu other;
	void Start () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		scaleGUI = Screen.width/1024;
		nextTrackRect = new Rect(975*scaleGUI,345*scaleGUI,69*scaleGUI,175*scaleGUI);
		lastTrackRect = new Rect(-20*scaleGUI,345*scaleGUI,69*scaleGUI,175*scaleGUI);
		wingsMiniRect = new Rect(480*scaleGUI, 50*scaleGUI, 61.5f*scaleGUI, 69.5f*scaleGUI);
		siteRect      = new Rect(458*scaleGUI, 700*scaleGUI, 95*scaleGUI, 37*scaleGUI);
		firstMapNameRect = new Rect(0*scaleGUI, 130*scaleGUI, 1024*scaleGUI, 57*scaleGUI);
		firstMap      = new Rect(450*scaleGUI,600*scaleGUI,122*scaleGUI,80*scaleGUI);
		lockerRect = new Rect(440*scaleGUI,290*scaleGUI,148*scaleGUI,185*scaleGUI);
	}
	
	void OnGUI () 
	{
		GUI.skin = scaleGUI==2? other.textStyle : other.textStyle2;
		GUI.Button (new Rect (150*scaleGUI,550*scaleGUI,100*scaleGUI*2,30*scaleGUI*2), "Yoar Goal:");
		string stringToEdit=other.GetBeetTime();
		GUI.Button (new Rect (150*scaleGUI,580*scaleGUI,100*scaleGUI*2,30*scaleGUI*2), "Beat Time: "+stringToEdit);
		stringToEdit=other.GetGoalDriftScore();
		GUI.Button (new Rect (150*scaleGUI,610*scaleGUI,100*scaleGUI*2,30*scaleGUI*2), "Drift Score: "+stringToEdit);
		if((other.loadMapCount>5 && !other.isPurchased))//>5
		{
			GUI.skin=lockerSkin;
			GUI.Label(lockerRect, " ");
		}
		else if(!other.IsUnlockLvl(other.loadMapCount) && other.isPurchased)
		{
			GUI.skin=lockerSkin;
			GUI.Label(lockerRect, " ");
			GUI.skin=playDisSkin;
			if(GUI.Button(firstMap," ")) //1
			{
			
    		}
		}
		else
		{
			if(other.IsUnlockLvl(other.loadMapCount))
			{		
				GUI.Button (new Rect (800*scaleGUI,550*scaleGUI,100*scaleGUI*2,30*scaleGUI*2), "Your Result:");
				stringToEdit=other.GetYourTime();
				GUI.Button (new Rect (800*scaleGUI,580*scaleGUI,100*scaleGUI*2,30*scaleGUI*2), "Time: "+stringToEdit);
				stringToEdit=other.GetYourDriftScore();
				GUI.Button (new Rect (800*scaleGUI,610*scaleGUI,100*scaleGUI*2,30*scaleGUI*2), "Drift Score: "+stringToEdit);
				GUI.skin=PlayGameSkin;
				if(GUI.Button(firstMap," ")) //1
				{
					other.clickS.Play();
					other.UnPause();
					other.gameMode=true;
					other.firstLoad=true;
					loader.bundle.Unload(true);
					if(other.isMiniMap) Application.LoadLevel(other.loadMapCount+MINI_MAP_CONST);
					else Application.LoadLevel(other.loadMapCount+NORMAL_MAP_CONST);
    			}
			}
		}
		
		
		GUI.skin=nextTrackSkin;
		if(GUI.Button(nextTrackRect,""))//next track
		{
			other.clickS.Play();
			if(other.loadMapCount<LAST_MAP_CONST)
			other.loadMapCount+=2;
    	}
		GUI.skin=lastTrackSkin;
		if(GUI.Button(lastTrackRect,""))//last track
		{
			other.clickS.Play();
			if(other.loadMapCount>FIRST_MAP_CONST)
			other.loadMapCount-=2;
    	}
		GUI.skin=backSkin;
		if(GUI.Button(new Rect(50*scaleGUI, 50*scaleGUI, 99.5f*scaleGUI, 57*scaleGUI), ""))//Back
		{
			other.backClickS.Play();
			other.trackChoiseMod = false;
			other.carChoiseMod=true;
			other.firstLoad=true;
			loader.bundle.Unload(true);
    	}
		GUI.skin=wingsMiniSkin;
    	if(GUI.Button(wingsMiniRect,""))//WingsMini
		{
    	}
		GUI.skin=siteSkin;
    	if(GUI.Button(siteRect,""))//Site 
		{
			other.clickS.Play();
    		Application.OpenURL("http://www.ardriftracing.com");
    	}
		GUI.skin=firstMapNameSkin;
    	if(GUI.Button(firstMapNameRect,""))//1
		{
    	}

		if(other.loadMapCount==1) //1
		{
			other.SetGUISkins(firstMapTexture);
		}
		if(other.loadMapCount==3) //2
		{
			other.SetGUISkins(secondMapTexture);
		}
		if(other.loadMapCount==5) //3
		{
			other.SetGUISkins(thirdMapTexture);
		}
		if(other.loadMapCount==7) //4
		{
			other.SetGUISkins(fourthMapTexture);
		}
		if(other.loadMapCount==9) //5
		{
			other.SetGUISkins(fifthMapTexture);
		}
		if(other.loadMapCount==11) //6
		{
			other.SetGUISkins(sixthMapTexture);
		}
		if(other.loadMapCount==13) //7
		{
			other.SetGUISkins(seventhMapTexture);
		}
		if(other.loadMapCount==15) //8
		{
			other.SetGUISkins(eighthMapTexture);
		}
		if(other.loadMapCount==17) //9
		{
			other.SetGUISkins(ninethMapTexture);
		}
		if(other.loadMapCount==19) //10
		{
			other.SetGUISkins(tenthMapTexture);
		}
		if(other.loadMapCount==21) //11
		{
			other.SetGUISkins(eleventhMapTexture);
		}
		if(other.loadMapCount==23) //12
		{
			other.SetGUISkins(twelvethMapTexture);
		}
		if(other.loadMapCount==25) //13
		{
			other.SetGUISkins(threeteenthMapTexture);
		}
		if(other.loadMapCount==27) //14
		{
			other.SetGUISkins(fourteenthMapTexture);
		}
		if(other.loadMapCount==29) //15
		{
			other.SetGUISkins(fifteenthMapTexture);
		}
		if(other.loadMapCount==31) //16
		{
			other.SetGUISkins(sixteenthMapTexture);
		}
		if(other.loadMapCount==33) //17
		{
			other.SetGUISkins(seventeenthMapTexture);
		}
		if(other.loadMapCount==35) //18
		{
			other.SetGUISkins(eighteenthMapTexture);
		}
		if(other.loadMapCount==37) //19
		{
			other.SetGUISkins(nineteenthMapTexture);
		}
		if(other.loadMapCount==39) //20
		{
			other.SetGUISkins(twentyMapTexture);
		}
		if(other.loadMapCount==41) //21
		{
			other.SetGUISkins(twentyoneMapTexture);
		}
		if(other.loadMapCount==43) //22
		{
			other.SetGUISkins(twentytwoMapTexture);
		}
		if(other.loadMapCount==45) //23
		{
			other.SetGUISkins(twentythreeMapTexture);
		}
		if(other.loadMapCount==47) //24
		{
			other.SetGUISkins(twentyfourMapTexture);
		}
		if(other.loadMapCount==49) //25
		{
			other.SetGUISkins(twentyfiveMapTexture);
		}
	}
}
