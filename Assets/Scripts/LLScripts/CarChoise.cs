/*
 * скрипт ленивой загрузки, подгружает меню выбора машин
 */
using UnityEngine;
using System.Collections;

public class CarChoise : MonoBehaviour 
{
	Rect wingsMiniRect;
	Rect nextTrackRect;
	Rect lastTrackRect;
	Rect firstCarStatRect;
	Rect secondCarStatRect;
	
	public GUISkin wingsMiniSkin;
	public GUISkin nextTrackSkin;
	public GUISkin lastTrackSkin;
	public GUISkin sliderSkin;
	public GUISkin firstCarStatStyle;
	public GUISkin secondCarStatStyle;
	
	public GUISkin wingsMiniSkin2;
	public GUISkin nextTrackSkin2;
	public GUISkin lastTrackSkin2;
	public GUISkin sliderSkin2;
	public GUISkin firstCarStatStyle2;
	public GUISkin secondCarStatStyle2;
	
	public Material firstCarMaterial;
	public Material secondCarMaterial;
	
	float hSliderValue = 1.0f;
	public Texture2D backGroundOn;
	public Texture2D backGroundOff;
	public Texture2D backGroundOn2;
	public Texture2D backGroundOff2;
	
	int scaleGUI;
	MainMenu other;
	void Start()
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		scaleGUI = Screen.width/1024;
		nextTrackRect = new Rect(975*scaleGUI,345*scaleGUI,69*scaleGUI,175*scaleGUI);
		lastTrackRect = new Rect(-20*scaleGUI,345*scaleGUI,69*scaleGUI,175*scaleGUI);
		wingsMiniRect = new Rect(480*scaleGUI, 50*scaleGUI, 61.5f*scaleGUI, 69.5f*scaleGUI);
		firstCarStatRect   = new Rect(390*scaleGUI,600*scaleGUI,261*scaleGUI,70*scaleGUI);
		secondCarStatRect  = new Rect(390*scaleGUI,600*scaleGUI,261*scaleGUI,70*scaleGUI);
		other.nextBackButton=true;
	}
	void OnGUI () 
	{
		GUI.skin= scaleGUI==2? wingsMiniSkin : wingsMiniSkin2;
    	if(GUI.Button(wingsMiniRect,""))//WingsMini
		{
    	}
		/*GUI.skin= scaleGUI==2? nextTrackSkin : nextTrackSkin2;
		if(GUI.Button(nextTrackRect,""))//next car
		{
			if(other.carCount==3) other.carCount=0;
			other.carCount++;
    	}
		GUI.skin= scaleGUI==2? lastTrackSkin : lastTrackSkin2;
		if(GUI.Button(lastTrackRect,""))//last car
		{
			if(other.carCount==1) other.carCount=4;
			other.carCount--;
    	}	*/	
		GUI.skin= scaleGUI==2? sliderSkin : sliderSkin2;
		//other.isHard = GUI.Toggle(new Rect(475*scaleGUI, 700*scaleGUI, 108*scaleGUI, 55*scaleGUI), other.isHard, "");
		hSliderValue = GUI.HorizontalSlider(new Rect (470*scaleGUI, 700*scaleGUI, 105*scaleGUI, 55*scaleGUI), hSliderValue, 0.0f, 1.0f);
		if(hSliderValue>0.5f) 
		{
			other.isHard = false;
			SetGUISkins(backGroundOff);
			if(Input.touchCount==0)
			hSliderValue = 1.0f;
		}
		else if(hSliderValue<=0.5f) 
		{
			other.isHard = true;
			SetGUISkins(backGroundOn);
			if(Input.touchCount==0)
			hSliderValue = 0.0f;
		}
		
		if(other.firstCar)
		{
			//RedCarMain();
			GameObject.Find("Plane").renderer.material = firstCarMaterial;
			GUI.skin= scaleGUI==2? firstCarStatStyle : firstCarStatStyle2;
			if(GUI.Button(firstCarStatRect,"")){}
		}
		if(other.secondCar)
		{
			//GreenCarMain();
			GameObject.Find("Plane").renderer.material = secondCarMaterial;
			GUI.skin= scaleGUI==2? secondCarStatStyle : secondCarStatStyle2;
			if(GUI.Button(secondCarStatRect,"")){}
		}
		if(other.thirdCar)
		{
			//BlueCarMain();
			GameObject.Find("Plane").renderer.material = firstCarMaterial;
			GUI.skin= scaleGUI==2? firstCarStatStyle : firstCarStatStyle2;
			if(GUI.Button(firstCarStatRect,"")){}
		}
	}
	public void SetGUISkins(Texture2D sTexture) //меняет картинки карт
	{
		sliderSkin.horizontalSlider.normal.background   = sTexture;
		sliderSkin.horizontalSlider.active.background   = sTexture;
		sliderSkin.horizontalSlider.hover.background    = sTexture;
		sliderSkin.horizontalSlider.focused.background  = sTexture;
		sliderSkin.horizontalSlider.onNormal.background = sTexture;
		sliderSkin.horizontalSlider.onActive.background = sTexture;
		sliderSkin.horizontalSlider.onHover.background  = sTexture;
		sliderSkin.horizontalSlider.onFocused.background= sTexture;
		
		sliderSkin2.horizontalSlider.normal.background   = sTexture;
		sliderSkin2.horizontalSlider.active.background   = sTexture;
		sliderSkin2.horizontalSlider.hover.background    = sTexture;
		sliderSkin2.horizontalSlider.focused.background  = sTexture;
		sliderSkin2.horizontalSlider.onNormal.background = sTexture;
		sliderSkin2.horizontalSlider.onActive.background = sTexture;
		sliderSkin2.horizontalSlider.onHover.background  = sTexture;
		sliderSkin2.horizontalSlider.onFocused.background= sTexture;
	}
}
