/*
 * скрипт ленивой загрузки меню хтп
 */
using UnityEngine;
using System.Collections;

public class HowToPlay : MonoBehaviour {
	Rect howToPlayTextRect;
	
	Rect printMarkerRect;
	Rect markerRect;
	Rect firstStepRect;
	Rect secondStepRect;
	Rect howToEnjoyRect;
	Rect wingsMiniRect;
	
	public GUISkin wingsMiniSkin;
	public GUISkin howToPlayTextSkin;

	public GUISkin printMarkerSkin;
	public GUISkin markerSkin;
	public GUISkin firstStepSkin;
	public GUISkin secondStepSkin;
	public GUISkin howToEnjoySkin;
	
	public GUISkin wingsMiniSkin2;
	public GUISkin howToPlayTextSkin2;
	public GUISkin printMarkerSkin2;
	public GUISkin markerSkin2;
	public GUISkin firstStepSkin2;
	public GUISkin secondStepSkin2;
	public GUISkin howToEnjoySkin2;
	
	int scaleGUI;
	
	void Start()
	{
		scaleGUI = Screen.width/1024;
		howToPlayTextRect= new Rect(0*scaleGUI, 130*scaleGUI, 1024*scaleGUI, 55*scaleGUI);//how to play
		firstStepRect    = new Rect(295*scaleGUI,220*scaleGUI,43*scaleGUI,43*scaleGUI);
		secondStepRect   = new Rect(675*scaleGUI,220*scaleGUI,42*scaleGUI,42*scaleGUI);
		howToEnjoyRect   = new Rect(600*scaleGUI,350*scaleGUI,183*scaleGUI,123*scaleGUI);
		
		printMarkerRect	 = new Rect(255*scaleGUI, 300*scaleGUI, 114*scaleGUI, 42*scaleGUI);
		markerRect     	 = new Rect(170*scaleGUI, 350*scaleGUI, 285*scaleGUI, 202*scaleGUI);
		
		wingsMiniRect = new Rect(480*scaleGUI, 50*scaleGUI, 61.5f*scaleGUI, 69.5f*scaleGUI);
	}
	string GetPath()
	{
		string root = Application.dataPath.Substring(0, Application.dataPath.Length - 26) + "/Documents";
		#if UNITY_EDITOR
		root = Application.dataPath.Substring(0, Application.dataPath.Length);
		#endif
        return root;
	}
	void OnGUI()
	{
		GUI.skin= scaleGUI==2? howToEnjoySkin : howToEnjoySkin2;
		if(GUI.Button(howToEnjoyRect,"")){}//HowToEnjoy
		
		GUI.skin= scaleGUI==2? firstStepSkin : firstStepSkin2;
		if(GUI.Button(firstStepRect,"")){}//First Step
		
		GUI.skin= scaleGUI==2? secondStepSkin : secondStepSkin2;
		if(GUI.Button(secondStepRect,"")){}//Second Step
		
		GUI.skin= scaleGUI==2? wingsMiniSkin : wingsMiniSkin2;
    	if(GUI.Button(wingsMiniRect,"")){}//WingsMini
		
		GUI.skin= scaleGUI==2? howToPlayTextSkin : howToPlayTextSkin2;
    	if(GUI.Button(howToPlayTextRect,"")){}//How To Play Text

		GUI.skin= scaleGUI==2? markerSkin : markerSkin2;
    	if(GUI.Button(markerRect,"")){}//Marker

		GUI.skin= scaleGUI==2? printMarkerSkin : printMarkerSkin2;
    	if(GUI.Button(printMarkerRect,""))//Print Marker
		{
    	    Application.OpenURL("http://www.ardriftracing.com");
    	}
	}
}
