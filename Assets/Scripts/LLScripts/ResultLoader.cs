/*
 * скрипт подгрузки бандла для результатов 
 */
using UnityEngine;
using System.Collections;

public class ResultLoader : MonoBehaviour {

	public GameObject go;
	public static AssetBundle bundle;
	static int count=1;
	IEnumerator Start () 
	{
		//string url ="file://"+Application.dataPath.Substring(0, Application.dataPath.Length - 13) + "/ardriftracingfree.app/QCAR/Result.unity3d";
    	string url ="file://"+Application.dataPath.Substring(0, Application.dataPath.Length - 4) + "QCAR/Result.unity3d";
		WWW www = WWW.LoadFromCacheOrDownload (url, count++);
    	yield return www;
    	bundle = www.assetBundle;
    	go = bundle.Load("Result", typeof(GameObject)) as GameObject;
    	Instantiate(go);
	}
}
