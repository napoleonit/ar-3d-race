using UnityEngine;
using System.Collections;

public class LeftJoyPosition : MonoBehaviour 
{
	MainMenu other;
	// Use this for initialization
	void Start () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		if(other.scaleGUI==1)
		{
			guiTexture.pixelInset = new Rect(guiTexture.pixelInset.x/2, guiTexture.pixelInset.y/2-30, guiTexture.pixelInset.width/2, guiTexture.pixelInset.height/2);
		}
	}
}
