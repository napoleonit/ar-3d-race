using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FirstStart : MonoBehaviour
{
    int k = 0;
	public GUISkin customSkin;
	public Rect cancelRect;
	public Rect backRect;
	public Rect nextRect;
	public Material[] materials;
	float timeToSwitch;
	float switchTime;
	bool finish;
	bool isSwitch;
	
	void Start()
	{
		SetTex(0);
		timeToSwitch=0;
		finish=true;
		isSwitch=false;
	}
	void Update()
	{
		if(k==2)finish=true;
		timeToSwitch+=Time.deltaTime;
		Debug.Log(timeToSwitch);
		if(timeToSwitch>4 && !finish)
		{
			isSwitch=true;
		}
		else if(timeToSwitch>4 && finish)
		{
			Application.LoadLevel("QCAR-Menu");
		}
		if(isSwitch)
		{
			k++;
			if(k>2)k=2;
			SetTex(k);
			timeToSwitch=0;
			isSwitch=false;
		}
	}
    void SetTex(int N)
    {
        renderer.sharedMaterial = materials[N];
    }
	void OnGUI()
	{
		if(GUI.Button(cancelRect, "Cancel"))
		{
    	    Application.LoadLevel("QCAR-Menu");
    	}
		if(k>0)
		{
			if(GUI.Button(backRect, "Back"))
			{
				k--;
				SetTex(k);
				timeToSwitch=0;
    		}
		}
		if(k<2)
		{
			if(GUI.Button(nextRect, "Next"))
			{
				k++;
				SetTex(k);
				timeToSwitch=0;
    		}
		}
	}
}