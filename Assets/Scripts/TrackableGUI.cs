using UnityEngine;
using System.Collections;

public class TrackableGUI : MonoBehaviour 
{
	
	DefaultTrackableEventHandler other;
	
	
	// Use this for initialization
	void Start () 
	{
		other = GameObject.Find("ImageTargetStones").GetComponent("DefaultTrackableEventHandler") as DefaultTrackableEventHandler;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(other.statusTracked)
		{
			this.gameObject.guiTexture.color = new Color(gameObject.guiTexture.color.r, gameObject.guiTexture.color.g, gameObject.guiTexture.color.b, 1f);
		}
		else
		{
			this.gameObject.guiTexture.color = new Color(gameObject.guiTexture.color.r, gameObject.guiTexture.color.g, gameObject.guiTexture.color.b, 0f);
		}
	}
}
