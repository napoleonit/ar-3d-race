using UnityEngine;
using System.Collections;

public class InverseStopJoy : MonoBehaviour {

	MainMenu other;
	void Awake () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		if(other.inverse && other.scaleGUI==2)
		{
			transform.position = new Vector3(0.15f,0.0f,0);
		}
		else if(other.inverse && other.scaleGUI==1)
		{
			transform.position = new Vector3(0.2f,0.0f,0);
		}
	}
}
