using UnityEngine;
using System.Collections;

public class dot1 : MonoBehaviour 
{
	FinishSecondScene otherFin;
	// Use this for initialization
	void Start () 
	{
		otherFin = GameObject.Find("Finish").GetComponent("FinishSecondScene") as FinishSecondScene;
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.name=="chassis22")
		{
			if(!otherFin.isCarParking) otherFin.dot[0]=true;
			else otherFin.dotCarParking=true;
		}
    }
}
