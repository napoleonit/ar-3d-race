using UnityEngine;
using System.Collections;

public class InverseForward : MonoBehaviour 
{
	MainMenu other;
	void Awake () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
		if(other.inverse)
		{
			transform.position = new Vector3(0.0f,0,0);
		}
	}

}
