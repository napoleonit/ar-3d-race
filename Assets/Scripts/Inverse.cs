using UnityEngine;
using System.Collections;

public class Inverse : MonoBehaviour 
{
	public GameObject back;
	public GameObject forward;
	public GameObject rotate;
	public GameObject rule;
	MainMenu other;
	
	void Start () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
	}
	
	void Update()
	{
		if(other.inverse)
		{
			back.transform.position = new Vector3(-0.7f,0,0);
			forward.transform.position = new Vector3(-0.9f,0,0);
			rotate.transform.position = new Vector3(0.8f,0.2f,0);
			rule.transform.position = new Vector3(398.7f,35.3126f,1.789512f);
		}
	}
}
