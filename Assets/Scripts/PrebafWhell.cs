using UnityEngine;
using System.Collections;
using System;

public class PrebafWhell : MonoBehaviour 
{

public	WheelCollider CorrespondingCollider;
public	GameObject SlipPrefab;
	float RotationValue;
	
public	AudioSource drift;
public	AudioClip Adrift;
public	int score;
public	float timeGame;
float tmp=-100f;
	
void Start()
{
		
		gameObject.AddComponent ("AudioSource");
		drift.loop = true;
		drift.playOnAwake = true;
		drift.clip = Adrift;
		drift.panLevel = 0;
		
		score=0;
		timeGame=0.0f;
}

void Drift()
	{
		if(drift.isPlaying) return;
		drift.Play();
	}

void Update () 
{
	timeGame+=Time.deltaTime;
		if(GameObject.Find("Car Ferrari 458 Italia N020511").audio.volume<0.9) drift.volume = 0.0f;
		else drift.volume = 0.4f;
		
	RaycastHit hit;
	Vector3 ColliderCenterPoint  = CorrespondingCollider.transform.TransformPoint( CorrespondingCollider.center );
	
	if ( Physics.Raycast( ColliderCenterPoint, -CorrespondingCollider.transform.up, out hit, (CorrespondingCollider.suspensionDistance + CorrespondingCollider.radius) ) ) 
	{
		transform.position = hit.point + (CorrespondingCollider.transform.up * CorrespondingCollider.radius);
	}
	else
	{
		transform.position = ColliderCenterPoint - (CorrespondingCollider.transform.up * CorrespondingCollider.suspensionDistance);
	}
	
	transform.rotation = CorrespondingCollider.transform.rotation * Quaternion.Euler( RotationValue, CorrespondingCollider.steerAngle, 0 );
	RotationValue += CorrespondingCollider.rpm * ( 360/60 ) * Time.deltaTime;
	
	WheelHit CorrespondingGroundHit;
	CorrespondingCollider.GetGroundHit(out CorrespondingGroundHit );
	if ( CorrespondingGroundHit.sidewaysSlip  > 2.0f || CorrespondingGroundHit.sidewaysSlip < -2.0f) 
	{
		if ( SlipPrefab ) 
		{
			Instantiate( SlipPrefab, CorrespondingGroundHit.point, Quaternion.identity );
			Drift();
			score++;
		}
	}
	else drift.Stop();
}

void OnGUI()
{
		//int intTime=timeGame;
		//string stringToEdit="";
		//stringToEdit=stringToEdit+score;
		//GUI.TextField (new Rect (20, 260, 100, 30), System.Convert.ToString(timeGame), 25);
		//stringToEdit="";
		//stringToEdit=stringToEdit+intTime;
		GUI.TextField (new Rect (20, 340, 100, 30), System.Convert.ToString(tmp), 25);
}
}
