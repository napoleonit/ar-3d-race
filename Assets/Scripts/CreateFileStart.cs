/*
 * при первом запуске создает файлы
 */
using UnityEngine;
using System.Collections;
using System.IO;

public class CreateFileStart : MonoBehaviour {
	string path;
	string clearMap;
	string firstLvl;
	string secondLvl;
	string thirdLvl;
	
	void Start () 
	{
		path = GetPath() + "/FirstStart.dat";
		clearMap = GetPath() + "/ClearMap.txt";
		firstLvl = GetPath() + "/firstLvl";
		secondLvl = GetPath() + "/secondLvl";
		thirdLvl = GetPath() + "/thirdLvl";
		
		if (!File.Exists(clearMap))
		{
        	using (StreamWriter sw = File.CreateText(clearMap))
        	{
        	    sw.WriteLine("1\n413.514\n0\n-0.7378041\n2\n413.514\n0\n-0.7378041\n3\n413.514\n0\n-0.7378041\n4\n413.514\n0\n-0.7378041\n5\n413.514\n0\n-0.7378041\n6\n413.514\n0\n-0.7378041\n7\n413.514\n0\n-0.7378041\n8\n413.514\n0\n-0.7378041\n9\n413.514\n0\n-0.7378041\n10\n413.514\n0\n-0.7378041\n11\n413.514\n0\n-0.7378041\n12\n413.514\n0\n-0.7378041\n13\n413.514\n0\n-0.7378041\n14\n413.514\n0\n-0.7378041\n15\n413.514\n0\n-0.7378041\n16\n413.514\n0\n-0.7378041\n17\n413.514\n0\n-0.7378041\n18\n413.514\n0\n-0.7378041\n19\n413.514\n0\n-0.7378041\n20\n413.514\n0\n-0.7378041\n");
				sw.Close();
        	}
		}
		if (!File.Exists(firstLvl))
		{
        	using (StreamWriter sw = File.CreateText(firstLvl))
        	{
        	    sw.WriteLine("0");
				sw.WriteLine("0");
				sw.Close();
        	}
		}
		if (!File.Exists(secondLvl))
		{
        	using (StreamWriter sw = File.CreateText(secondLvl))
        	{
        	    sw.WriteLine("0");
				sw.WriteLine("0");
				sw.Close();
        	}
		}
		if (!File.Exists(thirdLvl))
		{
        	using (StreamWriter sw = File.CreateText(thirdLvl))
        	{
        	    sw.WriteLine("0");
				sw.WriteLine("0");
				sw.Close();
        	}
		}
		Application.LoadLevel("QCAR-Menu");
	}
	
	string GetPath()
	{
		string root = Application.dataPath.Substring(0, Application.dataPath.Length - 26) + "/Documents";
		#if UNITY_EDITOR
		root = Application.dataPath.Substring(0, Application.dataPath.Length);
		#endif
        return root;
	}
}
