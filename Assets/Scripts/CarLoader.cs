/*
 * загружает только нужную машину
 */
using UnityEngine;
using System.Collections;

public class CarLoader : MonoBehaviour {
	MainMenu other;
	// Use this for initialization
	void Start () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(other.Car!=null && other.Car.name!=this.name)
		{
			Destroy(gameObject);
		}
	}
}
