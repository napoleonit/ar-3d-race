using UnityEngine;

public class ExportAssetBundles
{


    [UnityEditor.MenuItem("Assets/Build AssetBundle From Selection - Track dependencies")]
    static void ExportResource()
    {
        // Bring up save panel
        string path = UnityEditor.EditorUtility.SaveFilePanel("Save Resource", "", "New Resource", "unity3d");
        if (path.Length != 0)
        {
            // Build the resource file from the active selection.
            Object[] selection = UnityEditor.Selection.GetFiltered(typeof(Object), UnityEditor.SelectionMode.DeepAssets);
            UnityEditor.BuildPipeline.BuildAssetBundle(UnityEditor.Selection.activeObject, selection, path, UnityEditor.BuildAssetBundleOptions.CollectDependencies | UnityEditor.BuildAssetBundleOptions.CompleteAssets);
            UnityEditor.Selection.objects = selection;
        }
    }

    [UnityEditor.MenuItem("Assets/Build AssetBundle From Selection - Track dependencies iPhone")]
    static void ExportAssetBundleIOSComplete()
    {
        // Bring up save panel
        string path = UnityEditor.EditorUtility.SaveFilePanel("Save Resource", "", "New Resource", "unity3d");
        if (path.Length != 0)
        {
            // Build the resource file from the active selection.
            Object[] selection = UnityEditor.Selection.GetFiltered(typeof(Object), UnityEditor.SelectionMode.DeepAssets);
            //
            UnityEditor.BuildPipeline.BuildAssetBundle(UnityEditor.Selection.activeObject, selection, path, UnityEditor.BuildAssetBundleOptions.CollectDependencies | UnityEditor.BuildAssetBundleOptions.CompleteAssets, UnityEditor.BuildTarget.iPhone);
            UnityEditor.Selection.objects = selection;
        }
    }

    [UnityEditor.MenuItem("Assets/Build AssetBundle From Selection - Track dependencies Android")]
    static void ExportAssetBundleAndroidComplete()
    {
        // Bring up save panel
        string path = UnityEditor.EditorUtility.SaveFilePanel("Save Resource", "", "New Resource", "unity3d");
        if (path.Length != 0)
        {
            // Build the resource file from the active selection.
            Object[] selection = UnityEditor.Selection.GetFiltered(typeof(Object), UnityEditor.SelectionMode.DeepAssets);
            //
            UnityEditor.BuildPipeline.BuildAssetBundle(UnityEditor.Selection.activeObject, selection, path, UnityEditor.BuildAssetBundleOptions.CollectDependencies | UnityEditor.BuildAssetBundleOptions.CompleteAssets, UnityEditor.BuildTarget.Android);
            UnityEditor.Selection.objects = selection;
        }
    }

    [UnityEditor.MenuItem("Assets/Build AssetBundle From Selection - No dependency tracking")]
    static void ExportResourceNoTrack()
    {
        // Bring up save panel
        string path = UnityEditor.EditorUtility.SaveFilePanel("Save Resource", "", "New Resource", "unity3d");
        if (path.Length != 0)
        {
            // Build the resource file from the active selection.
            UnityEditor.BuildPipeline.BuildAssetBundle(UnityEditor.Selection.activeObject, UnityEditor.Selection.objects, path);
        }
    }
}