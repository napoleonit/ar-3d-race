using UnityEngine;
using System.Collections;

public class DriftBack : MonoBehaviour 
{
	public WheelCollider col;
	public Joystick moveTouchPad;
	public Joystick backTouchPad;
	public Joystick stopKranTouchPad;
	MainMenu other;
	// Use this for initialization
	void Start () 
	{
		other = GameObject.Find("Menu and Settings").GetComponent("MainMenu") as MainMenu;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(other.isHard)
		{
			if(moveTouchPad.position.y > 0.01 || backTouchPad.position.y > 0.01) //Газ
			{
				WheelFrictionCurve faa=col.sidewaysFriction;// боковое трение
				faa.stiffness=0.036f;
				faa.asymptoteSlip=2;
				faa.asymptoteValue=4000;
				faa.extremumSlip=1;
				faa.extremumValue=3000;
				col.sidewaysFriction=faa;
				
				WheelFrictionCurve forwFric=col.forwardFriction; // форвард трение
				forwFric.stiffness=1f;
				forwFric.asymptoteSlip=2;
				forwFric.asymptoteValue=10000;
				forwFric.extremumSlip=1;
				forwFric.extremumValue=50000;
				col.forwardFriction=forwFric;
			}
			else if(stopKranTouchPad.position.y > 0.01)     //  РУЧНИК
			{
				WheelFrictionCurve faa=col.sidewaysFriction;// боковое трение
				faa.stiffness=0.02f;
				faa.asymptoteSlip=2;
				faa.asymptoteValue=4000;
				faa.extremumSlip=1;
				faa.extremumValue=3000;
				col.sidewaysFriction=faa;
				
				WheelFrictionCurve forwFric=col.forwardFriction; // форвард трение
				forwFric.stiffness=1f;
				forwFric.asymptoteSlip=2;
				forwFric.asymptoteValue=10000;
				forwFric.extremumSlip=1;
				forwFric.extremumValue=50000;
				col.forwardFriction=forwFric;
			}
			else //Стоим
			{
				WheelFrictionCurve faa=col.sidewaysFriction;// боковое трение
				faa.stiffness=0.06f;
				faa.asymptoteSlip=2;
				faa.asymptoteValue=4000;
				faa.extremumSlip=1;
				faa.extremumValue=3000;
				col.sidewaysFriction=faa;
				
				WheelFrictionCurve forwFric=col.forwardFriction; // форвард трение
				forwFric.stiffness=1f;
				forwFric.asymptoteSlip=2;
				forwFric.asymptoteValue=10000;
				forwFric.extremumSlip=1;
				forwFric.extremumValue=50000;
				col.forwardFriction=forwFric;
			}
		}
		else
		{
			/*
			if(moveTouchPad.position.y > 0.01 || backTouchPad.position.y > 0.01) //Газ
			{
				
				WheelFrictionCurve faa=col.sidewaysFriction;// боковое трение
				faa.stiffness=0.04f;
				faa.asymptoteSlip=2;
				faa.asymptoteValue=3000;
				faa.extremumSlip=1;
				faa.extremumValue=4000;
				col.sidewaysFriction=faa;
				
				WheelFrictionCurve forwFric=col.forwardFriction; // форвард трение
				forwFric.stiffness=0.2f;
				forwFric.asymptoteSlip=2;
				forwFric.asymptoteValue=10000;
				forwFric.extremumSlip=1;
				forwFric.extremumValue=50000;
				col.forwardFriction=forwFric;
				
				
			}
			else if(stopKranTouchPad.position.y > 0.01)     //  РУЧНИК
			{
				WheelFrictionCurve side=col.sidewaysFriction;// боковое трение
				side.stiffness=0.05f;
				side.asymptoteSlip=2;
				side.asymptoteValue=3000;
				side.extremumSlip=1;
				side.extremumValue=4000;
				col.sidewaysFriction=side;
				
				WheelFrictionCurve forw=col.forwardFriction; // форвард трение
				forw.stiffness=1f;
				forw.asymptoteSlip=2;
				forw.asymptoteValue=3000;
				forw.extremumSlip=1;
				forw.extremumValue=4000;
				col.forwardFriction=forw;
				
			}
			else //Стоим
			{
				WheelFrictionCurve side=col.sidewaysFriction;// боковое трение
				side.stiffness=0.05f;
				side.asymptoteSlip=2;
				side.asymptoteValue=3000;
				side.extremumSlip=1;
				side.extremumValue=4000;
				col.sidewaysFriction=side;
				
				WheelFrictionCurve forw=col.forwardFriction; // форвард трение
				forw.stiffness=1f;
				forw.asymptoteSlip=2;
				forw.asymptoteValue=3000;
				forw.extremumSlip=1;
				forw.extremumValue=4000;
				col.forwardFriction=forw;
				
			}
			//*/
				
				
				WheelFrictionCurve side=col.sidewaysFriction;// боковое трение
				side.stiffness=0.05f;
				side.asymptoteSlip=2;
				side.asymptoteValue=3000;
				side.extremumSlip=1;
				side.extremumValue=4000;
				col.sidewaysFriction=side;
				
				WheelFrictionCurve forw=col.forwardFriction; // форвард трение
				forw.stiffness=1f;
				forw.asymptoteSlip=2;
				forw.asymptoteValue=3000;
				forw.extremumSlip=1;
				forw.extremumValue=4000;
				col.forwardFriction=forw;
				
		}
	}
}
