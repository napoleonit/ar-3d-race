//
//  SKProduct2.h
//  interactivepdf
//
//  Created by Nap1 on 12.10.12.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@interface SKProduct (LocalizedPrice) 

@property (nonatomic, readonly) NSString *localizedPrice;
//@property (nonatomic, assign) BOOL isPurchased;
@property (nonatomic, readonly) NSString *localizedDuration;

@end
