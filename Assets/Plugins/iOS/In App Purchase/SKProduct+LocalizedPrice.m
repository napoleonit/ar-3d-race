//
//  SKProduct2.m
//  interactivepdf
//
//  Created by Nap1 on 12.10.12.
//
//

#import "SKProduct+LocalizedPrice.h"

@implementation SKProduct (LocalizedPrice)

//@dynamic isPurchased;

- (NSString *)localizedPrice
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:self.priceLocale];
#if RUB
    [numberFormatter setCurrencySymbol:@"₷"];
#endif
    NSString *formattedString = [numberFormatter stringFromNumber:self.price];
    [numberFormatter release];
    return formattedString;
}

- (NSString *)localizedDuration
{
    //dot-separated components. Last is the duration for subscription
    NSArray *components = [self.productIdentifier componentsSeparatedByString:@"."];
    NSString *strDuration = [components objectAtIndex:[components count]-1];
    
    NSString *formattedString;
    if ([strDuration isEqualToString:@"7days"]) {
        formattedString = @"7 дней";
        
    } else if ([strDuration isEqualToString:@"1month"]) {
        formattedString = @"1 месяц";
        
    } else if ([strDuration isEqualToString:@"2months"]) {
        formattedString = @"2 месяца";
        
    } else if ([strDuration isEqualToString:@"3months"]) {
        formattedString = @"3 месяца";
        
    } else if ([strDuration isEqualToString:@"6months"]) {
        formattedString = @"6 месяцев";
        
    } else if ([strDuration isEqualToString:@"1year"]) {
        formattedString = @"1 год";
        
    //not a subscription
    } else {
        formattedString=nil;
    }
    return formattedString;
}

@end
