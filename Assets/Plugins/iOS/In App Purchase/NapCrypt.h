//
//  NapCrypt.h
//  interactivepdf
//
//  Created by Nap1 on 16.10.12.
//
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>
#import <CommonCrypto/CommonKeyDerivation.h>

//*
@interface NapCrypt : NSObject

+ (NSData *)encryptedDataForData:(NSData *)data
                        password:(NSString *)password
                              iv:(NSData **)iv
                            salt:(NSData **)salt
                           error:(NSError **)error;
+ (NSData *)randomDataOfLength:(size_t)length;
+ (NSData *)AESKeyForPassword:(NSString *)password
                         salt:(NSData *)salt;

+(NSString*) getTokenFromHash:(NSString*)hashString withDate:(NSDate*)date;
+(NSString*) makeHashWithToken:(NSString*)token withDate:(NSDate*)date;

+(void)saveLocalTimeOffsetFromHeaders:(NSDictionary*) headers;

@end
//*/

@interface NSData (AESCrypt)

- (NSData *)AES256EncryptWithKey:(NSString *)key;
- (NSData *)AES256DecryptWithKey:(NSString *)key;

- (NSData *)ECB256EncryptWithKey:(NSString *)key;
- (NSData *)ECB256DecryptWithKey:(NSString *)key;

+ (NSData *)dataWithBase64EncodedString:(NSString *)string;
- (id)initWithBase64EncodedString:(NSString *)string;

- (NSString *)base64Encoding;
- (NSString *)base64EncodingWithLineLength:(NSUInteger)lineLength;

- (BOOL)hasPrefixBytes:(const void *)prefix length:(NSUInteger)length;
- (BOOL)hasSuffixBytes:(const void *)suffix length:(NSUInteger)length;

@end