//
//  InAppPurchasePlugin.m
//  interactivepdf
//
//  Created by Nap1 on 11.10.12.
//
//

#import "InAppPurchasePlugin.h"

@implementation InAppPurchasePlugin

static InAppPurchasePlugin * _sharedPlugin;

@synthesize products = myProducts;
@synthesize purchasedProducts = _purchasedProducts;
@synthesize request = _request;

- (id)init
{
    self = [super init];
    if (self) {
        myProducts = nil;
    }
    return self;
}

- (void)dealloc
{
    [myProducts release];
    
    [_productIdentifiers release];
    [_purchasedProducts release];
    [_request release];
    [super dealloc];
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    if ((self = [super init])) {
        self.productIdentifiers = productIdentifiers;
    }
    return self;
}

-(void)setProductIdentifiers:(NSSet *)productIdentifiers {
    if (_productIdentifiers==productIdentifiers) {
        return;
    }
    
    // Check for previously purchased products
    NSMutableSet * purchasedProducts = [NSMutableSet set];
    for (NSString * productIdentifier in productIdentifiers) {
        //We skip getting info about all purchased non-subscriptions
        if ([self checkIfPurchasedProductId:productIdentifier] && ![PaidSubscriptionProductIds containsObject:productIdentifier]) {
            [purchasedProducts addObject:productIdentifier];
            NSLog(@"Previously purchased: %@", productIdentifier);
        } else {
            NSLog(@"Not purchased: %@ or a subscription", productIdentifier);
        }
    }
    self.purchasedProducts = purchasedProducts;
    
    //skip purchased products and previously requested
    NSSet *filteredProductIds = [productIdentifiers objectsPassingTest:^BOOL(NSString* productId, BOOL *stop) {
        return !([purchasedProducts containsObject:productId] || [_productIdentifiers containsObject:productId]);
    }];
//    NSLog(@"%@", filteredProductIds);
    
    // Store product identifiers
    [_productIdentifiers release];
    _productIdentifiers = [filteredProductIds retain];
}

-(NSSet *)productIdentifiers {
    return _productIdentifiers;
}

+(id)sharedPlugin {
    if (_sharedPlugin != nil) {
        return _sharedPlugin;
    }
    
    _sharedPlugin = [[InAppPurchasePlugin alloc] init];
    [[SKPaymentQueue defaultQueue] addTransactionObserver:_sharedPlugin];
    
    return _sharedPlugin;
}

+(id)sharedPluginWithProductIdentifiers:(NSSet *)productIdentifiers {
    InAppPurchasePlugin *currentPlugin = [InAppPurchasePlugin sharedPlugin];
    
    currentPlugin.productIdentifiers = productIdentifiers;
//    currentPlugin.products = nil;
    
    return currentPlugin;
}

#pragma mark Main operating function

-(void)getProductData {
    //Do not send empty requests to apple
    if (_productIdentifiers==nil || [_productIdentifiers count]==0) {
        return;
    }
    
    NSLog(@"Getting data for product ids: %@", _productIdentifiers);
    
    //Create and initialize a products request object with the above list
    _request = [[SKProductsRequest alloc] initWithProductIdentifiers: _productIdentifiers];
//    _request = [[SKProductsRequest alloc] initWithProductIdentifiers: [NSSet setWithObject:@"ru.naked_science.201303"]];
    
    //Attach the request to your delegate
    _request.delegate = self;
    
    //Send the request to the App Store
    [_request start];
}

-(void)purchaseProductWithIdentifier:(NSString *)identifier {
    if ([identifier isEqualToString:@""]) {
        return;
    }
    NSLog(@"Buying %@...", identifier);
    if ([identifier isEqualToString:FreeSubscriptionProductId] || [PaidSubscriptionProductIds indexOfObject:identifier]!=NSNotFound) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kProductStartSubscribingNotification object:identifier];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kProductStartPurchasingNotification object:identifier];
    }

    SKMutablePayment *payment = [SKMutablePayment paymentWithProductIdentifier:identifier];;
    payment.quantity = 1;
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

+(BOOL)canMakePayments {
    return [SKPaymentQueue canMakePayments];
}

-(BOOL)checkIfPurchasedProductId:(NSString *)productId {
//        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] dictionaryRepresentation]);
//        BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
    NSDictionary* purchasedDict = [[NSUserDefaults standardUserDefaults] objectForKey:productId];
//        NSLog(@"%@", purchasedDict);
    if ([purchasedDict isKindOfClass:[NSDictionary class]] && purchasedDict!=nil && [purchasedDict objectForKey:@"token"]!=nil) {
        return YES;
    } else {
        return NO;
    }

}

-(BOOL)checkIfPurchasedSubscriptionForDate:(NSDate *)targetDate forProductId:(NSString *)productId {
    BOOL purchased = NO;
    
    //Iterate through all subscriptions and check stored dates
    for (NSString*curSubscriptionId in PaidSubscriptionProductIds) {
        NSDictionary* purchasedDict = [[NSUserDefaults standardUserDefaults] objectForKey:curSubscriptionId];
        
//        NSLog(@"%@", purchasedDict);
        //If subscription is purchased
        if ([purchasedDict isKindOfClass:[NSDictionary class]] && purchasedDict!=nil && [purchasedDict objectForKey:@"token"]!=nil) {
            NSArray *datesArray = [purchasedDict objectForKey:@"dates"];
            if (datesArray) {
                for (NSDictionary*dateDict in datesArray) {
                    NSDate *startDate = [dateDict objectForKey:@"startDate"];
                    NSDate *endDate = [dateDict objectForKey:@"endDate"];
                    //If target date is later than start date and earlier than end date, it's the correct subscription
                    if ([[targetDate laterDate:startDate] isEqualToDate:[targetDate earlierDate:endDate]]) {
                        purchased = YES;
                        
                        //store subscription
                        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
                        dict[@"id"] = purchasedDict[@"id"];
                        dict[@"date"] = purchasedDict[@"date"];
                        dict[@"productID"] = purchasedDict[@"productID"];
                        dict[@"token"] = purchasedDict[@"token"];
                        
//                        NSLog(@"%@", dict);
                        //Store to required product id
                        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:productId];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        [dict release];
                        
                        //stop iterating
                        break;
                    }
                }
                if (purchased) {
                    break;
                }
            }
            
        //Not purchased subscription
        } else {
            continue;
        }
    }
    
    return purchased;
}

-(NSString*) extractTokenForProductId:(NSString*) productId {
    NSDictionary* purchasedDict = [[NSUserDefaults standardUserDefaults] objectForKey:productId];
//    NSLog(@"%@", purchasedDict);
    
    //Here we make everything if
    if ([purchasedDict isKindOfClass:[NSDictionary class]] && purchasedDict!=nil && [purchasedDict objectForKey:@"token"]!=nil) {
        
        NSString *hash = [purchasedDict objectForKey:@"token"];
        NSDate *date = [purchasedDict objectForKey:@"date"];
//        hash = @"UcyGR2YfZY7QmYi++D6p46TXbibbfbRahl0ZyW+LBmw=";
        NSString *token = [[InAppPurchasePlugin sharedPlugin] getTokenFromHash:hash withDate:date];
//        NSLog(@"%@", token);
        NSString *requestToken = [[InAppPurchasePlugin sharedPlugin] makeHashWithToken:token withDate:[NSDate date]];
        
        return requestToken;
   } else {
        return nil;
    }
    
 }

-(void) invalidatePurchasedProductID:(NSString*)productId {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:productId];
}

-(NSString*)getHashFromTransaction:(SKPaymentTransaction *)transaction
{
    NSString *receiptString = [[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding];
//    NSLog(@"Sending sync transaction info:\nproductId=%@\ntransId=%@\nDate=%@\nReceiptstring=%@",transaction.payment.productIdentifier, transaction.transactionIdentifier, transaction.transactionDate, receiptString);
    
#if DEBUG!=1
    NSString* stringURL=[NSString stringWithFormat:@"%@/aravto/trans.php",ServerName];
#else
    NSString* stringURL=[NSString stringWithFormat:@"%@/aravto/trans_debug.php",ServerName];
#endif
    
    ASIFormDataRequest* req=[[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:stringURL]];
    [req addPostValue:transaction.transactionIdentifier forKey:@"transactionIdentifier"];
    [req addPostValue:receiptString forKey:@"transactionReceipt"];
    [req addPostValue:transaction.transactionDate forKey:@"transactionDate"];
    [req addPostValue:transaction.payment.productIdentifier forKey:@"productID"];
    [req startSynchronous];
    
    NSDictionary*tokenDict = [[req responseData] objectFromJSONData];
    NSString *responseString = [req responseString];
    NSLog(@"Received token: %@\nReceived string: %@", tokenDict, responseString);
    [receiptString release];
    [req release]; //vasleakage
    
    return [tokenDict objectForKey:@"token"];
}

-(NSDictionary*) getDatesFromTransaction:(SKPaymentTransaction*) transaction {
    //Get dictionary from data
    NSPropertyListFormat format;
    NSDictionary *jsonReceipt = [NSPropertyListSerialization propertyListFromData:transaction.transactionReceipt mutabilityOption:NSPropertyListImmutable format:&format errorDescription:nil];
    
    if (!jsonReceipt) {
        NSLog(@"BUG HERE!!! Could not get dictionary from transaction receipt: %@", [[[NSString alloc] initWithData:transaction.transactionReceipt encoding:NSUTF8StringEncoding] autorelease]);
        return nil;
    }
    
    NSString *purchaseInfo = [jsonReceipt objectForKey:@"purchase-info"];
    NSDictionary *jsonPurchaseInfo = [NSPropertyListSerialization propertyListFromData:[NSData dataWithBase64EncodedString:purchaseInfo] mutabilityOption:NSPropertyListImmutable format:&format errorDescription:nil];
    
    if (!jsonPurchaseInfo) {
        NSLog(@"BUG HERE!!! Could not get dictionary from purchase info: %@", jsonReceipt);
        return nil;
    }
//    NSLog(@"purchase info decoded dict: %@", jsonPurchaseInfo);
    
    //Convert strings to NSDates
    NSDateFormatter *stringToDate = [[NSDateFormatter alloc] init];
    [stringToDate setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [stringToDate setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //Remove " Etc/GMT" at the end of the strings
    NSString *startDateStr = [jsonPurchaseInfo[@"purchase-date"] substringToIndex:[jsonPurchaseInfo[@"purchase-date"] length]-8];
    NSString *endDateStr = [jsonPurchaseInfo[@"expires-date-formatted"] substringToIndex:[jsonPurchaseInfo[@"purchase-date"] length]-8];
    
    NSDate *startDate = [stringToDate dateFromString:startDateStr];
    NSDate *endDate = [stringToDate dateFromString:endDateStr];
//    startDate = [stringToDate dateFromString:@"2012-01-01 00:00:00"];
//    endDate = [stringToDate dateFromString:@"2012-11-30 00:00:00"];
    [stringToDate release];
    
    //If there's some error
    if (!startDate || !endDate) {
        NSLog(@"BUG HERE!!! Could not extract dates from purchase info: %@", jsonPurchaseInfo);
        return nil;
    }
    NSDictionary *datesDict = @{@"startDate" : startDate, @"endDate" : endDate};
    
    return datesDict;
}

-(void) restorePurchases {
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductStartRestoringNotification object:nil];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

-(void) storeTransaction:(SKPaymentTransaction*)transaction withHash:(NSString*)hash {
    SKPaymentTransaction *transactionToStore;
    
    //store only original transaction if it exists
    if (transaction.originalTransaction!=nil) {
        transactionToStore = transaction.originalTransaction;
    } else {
        transactionToStore = transaction;
    }

    NSDictionary *storedDict = [[NSUserDefaults standardUserDefaults] objectForKey:transactionToStore.payment.productIdentifier];
    NSMutableDictionary *dict;
    if (storedDict!=nil) {
        dict = [[NSMutableDictionary alloc] initWithDictionary:storedDict];
    } else {
        dict = [[NSMutableDictionary alloc] init];
    }
    dict[@"id"] = transactionToStore.transactionIdentifier;
    dict[@"date"] = transactionToStore.transactionDate;
    dict[@"productID"] = transactionToStore.payment.productIdentifier;
    if (hash) {
        dict[@"token"] = hash;
    }
    
    //Store additional info
    if ([PaidSubscriptionProductIds containsObject:transactionToStore.payment.productIdentifier]) {
        //Check if datesArray is already created
        NSMutableArray *datesArray = [NSMutableArray arrayWithArray:[dict objectForKey:@"dates"]];

        //Check if we already have this time interval
        NSDictionary *newDates = [self getDatesFromTransaction:transaction];
        //If we don't have it - add it
        if (newDates && ![datesArray containsObject:newDates]) {
            [datesArray addObject:newDates];
        }
        [dict setObject:datesArray forKey:@"dates"];
    }
    
//    NSLog(@"%@", dict);
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:transaction.payment.productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [dict release];
    [_purchasedProducts addObject:transaction.payment.productIdentifier];
}

#pragma mark Token workflow

-(NSString *)getTokenFromHash:(NSString *)hashString withDate:(NSDate *)date {
    return [NapCrypt getTokenFromHash:hashString withDate:date];
}

-(NSString *)makeHashWithToken:(NSString *)token withDate:(NSDate *)date {
    return [NapCrypt makeHashWithToken:token withDate:date];
}

#pragma mark SKProductsRequestDelegate methods

-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    if (myProducts!=nil) {
        [myProducts addObjectsFromArray:response.products];
    } else {
        myProducts = [response.products mutableCopy];
    }
    [_request release]; //_request=nil;
    
//    NSLog(@"%@", myProducts);
    for (int i=0; i<[myProducts count]; i++) {
        SKProduct *proUpgradeProduct = [myProducts objectAtIndex:i];

        NSLog(@"Product info:\nTitle: %@\nDescription: %@\nPrice: %@\nLocalized price: %@\nLocalized duration: %@\nID: %@" , proUpgradeProduct.localizedTitle, proUpgradeProduct.localizedDescription, proUpgradeProduct.price, proUpgradeProduct.localizedPrice, proUpgradeProduct.localizedDuration, proUpgradeProduct.productIdentifier);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductsLoadedNotification object:myProducts];
}

#pragma mark SKPaymentTransactionObserver methods

-(void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    for (SKPaymentTransaction *transaction in transactions)
    {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    }
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"completeTransaction...");
    
    [self recordTransaction: transaction];
//    [self provideContent: transaction.payment.productIdentifier];
    // Remove the transaction from the payment queue.
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchasedNotification object:transaction.payment.productIdentifier];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) restoreTransaction: (SKPaymentTransaction *)transaction
{
    NSLog(@"restoreTransaction %@...", transaction.payment.productIdentifier);

    [self recordTransaction: transaction];
//    [self provideContent: transaction.originalTransaction.payment.productIdentifier];
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductRestoredNotification object:transaction.payment.productIdentifier];
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void) failedTransaction: (SKPaymentTransaction *)transaction
{
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
        NSError *error = transaction.error;
        [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseFailedNotification object:transaction.error];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseCancelledNotification object:transaction];
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

- (void)recordTransaction:(SKPaymentTransaction *)transaction {
    // Optional: Record the transaction on the server side...
    NSString *hash = [self getHashFromTransaction:transaction];
    [self storeTransaction:transaction withHash:hash];
//    NSLog(@"%@",transaction.downloads);
}

- (void)provideContent:(NSString *)productIdentifier {
    
    NSLog(@"Toggling flag for: %@", productIdentifier);
    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [_purchasedProducts addObject:productIdentifier];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchasedNotification object:productIdentifier];
    
}

-(void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    if (error.code == SKErrorPaymentCancelled) {
        NSLog(@"Cancelled restoring");
        [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseCancelledNotification object:nil];
    } else {
        NSLog(@"Failed restoring");
        [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchaseFailedNotification object:error];
    }
}

-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    NSLog(@"Completed restoring");
    [[NSNotificationCenter defaultCenter] postNotificationName:kProductPurchasedNotification object:nil];
}

@end
