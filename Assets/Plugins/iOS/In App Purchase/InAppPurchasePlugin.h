//
//  InAppPurchasePlugin.h
//  interactivepdf
//
//  Created by Nap1 on 11.10.12.
//
//

#import <Foundation/Foundation.h>
#import "SKProduct+LocalizedPrice.h"
#import <CommonCrypto/CommonCrypto.h>
#import "NapCrypt.h"
#import "JSONKit.h"
#import "ASIFormDataRequest.h"

#define ServerName @"http://itnap.ru"
#define InAppPurchaseSalt @"8xIcmezAdmNu"
#define FreeSubscriptionProductId @""
#define PaidSubscriptionProductIds @[@""]

#define kProductsLoadedNotification @"ProductsLoaded"
#define kProductStartPurchasingNotification       @"ProductStartedPurchasing"
#define kProductPurchasedNotification       @"ProductPurchased"
#define kProductStartSubscribingNotification       @"ProductStartedSubscribing"
#define kProductStartRestoringNotification       @"ProductStartedRestoring"
#define kProductRestoredNotification       @"ProductRestored"
#define kProductPurchaseFailedNotification  @"ProductPurchaseFailed"
#define kProductPurchaseCancelledNotification  @"ProductPurchaseCancelled"


@interface InAppPurchasePlugin : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    SKProductsRequest *_request;
    NSMutableArray* myProducts;
    
    NSSet * _productIdentifiers;
    NSMutableSet * _purchasedProducts;
}

@property (retain) NSSet *productIdentifiers;
@property (retain) NSArray * products;
@property (retain) NSMutableSet *purchasedProducts;
@property (retain) SKProductsRequest *request;

+(id)sharedPlugin;
+(id)sharedPluginWithProductIdentifiers:(NSSet *)productIdentifiers;

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;

-(void)getProductData;

-(void) purchaseProductWithIdentifier:(NSString *)productIdentifier;

+(BOOL)canMakePayments;

-(NSString*)getHashFromTransaction:(SKPaymentTransaction *)transaction;

-(NSString*) extractTokenForProductId:(NSString*) productId;

-(BOOL) checkIfPurchasedProductId:(NSString*)productId;
-(BOOL) checkIfPurchasedSubscriptionForDate:(NSDate*)journalDate forProductId:(NSString*)productId;
-(void) invalidatePurchasedProductID:(NSString*)productId;
-(void) restorePurchases;

-(NSString*) getTokenFromHash:(NSString*)hashString withDate:(NSDate*)date;
-(NSString*) makeHashWithToken:(NSString*)token withDate:(NSDate*)date;

@end
