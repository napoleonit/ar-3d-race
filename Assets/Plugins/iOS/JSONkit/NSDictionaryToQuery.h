//
//  NSDictionaryToQuery.h
//  ShareSocialNet
//
//  Created by Nap1 on 02.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionaryToQuery : NSDictionary

@end

@interface NSDictionary (NSDictionaryToQuery)
- (NSString*) dictionaryToURLQuery:(NSString *)objectSeparator withKVSeparator:(NSString*) KVSeparator;
- (NSString*) dictionaryToURLQuery: (NSString*)objectSeparator;
- (NSString*) dictionaryToURLQuery;
@end
