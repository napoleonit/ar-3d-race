//
//  NSDictionaryToQuery.m
//  ShareSocialNet
//
//  Created by Nap1 on 02.05.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NSDictionaryToQuery.h"

@implementation NSDictionaryToQuery

@end

static NSString *toString(id object) {
    return [NSString stringWithFormat: @"%@", object];
}

// helper function: get the url encoded string form of any object
static NSString *urlEncode(id object) {
    NSString *string = toString(object);
    return [string stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
}

@implementation NSDictionary (NSDictionaryToQuery)

- (NSString*) dictionaryToURLQuery:(NSString *)objectSeparator withKVSeparator:(NSString*) KVSeparator {
    NSMutableArray *allKeys = [NSMutableArray arrayWithArray:[self allKeys]];
    [allKeys sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSMutableArray *parts = [NSMutableArray array];
    NSEnumerator* enumerator = [allKeys objectEnumerator];
    for (id key in enumerator) {
        id value = [self objectForKey: key];
        NSString *part = [NSString stringWithFormat: @"%@%@%@", urlEncode(key), KVSeparator, urlEncode(value)];
        [parts addObject: part];
    }
return [parts componentsJoinedByString: [NSString stringWithFormat:@"%@", objectSeparator]];
}

- (NSString*) dictionaryToURLQuery: (NSString*)objectSeparator {
    return [self dictionaryToURLQuery:objectSeparator withKVSeparator:@"="];
}

- (NSString*) dictionaryToURLQuery {
    return [self dictionaryToURLQuery:@"&"];
}

@end