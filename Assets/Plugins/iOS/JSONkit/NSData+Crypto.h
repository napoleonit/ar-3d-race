//
//  NSString.h
//  iMission
//
//  Created by Nap on 27.03.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/NSData.h>
#import <CommonCrypto/CommonDigest.h>
@interface NSData(Hash)
-(NSString*)md5Hash;
-(NSString*)sha1Hash;
@end
