//
//  UnityPlugin.m
//  unityPlugin
//
//  Created by Nap1 on 25.02.13.
//  Copyright (c) 2013 Napoleon IT. All rights reserved.
//

#import "UnityPlugin.h"

static UnityPlugin* _sharedPlugin;

// Converts C style string to NSString
NSString* CreateNSString (const char* string)
{
	if (string)
		return [NSString stringWithUTF8String: string];
	else
		return [NSString stringWithUTF8String: ""];
}

// Helper method to create C string copy
char* MakeStringCopy (const char* string)
{
	if (string == NULL)
		return NULL;
	
	char* res = (char*)malloc(strlen(string) + 1);
	strcpy(res, string);
	return res;
}


@implementation UnityPlugin

- (id)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productsLoaded:) name:kProductsLoadedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startedPurchasing:) name:kProductStartPurchasingNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:kProductPurchasedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startedRestoring:) name:kProductStartRestoringNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productRestored:) name:kProductRestoredNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchaseCancelled:) name:kProductPurchaseCancelledNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchaseFailed:) name:kProductPurchaseFailedNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startedSubscribing:) name:kProductStartSubscribingNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

+(id)sharedPlugin {
    if (_sharedPlugin != nil) {
        return _sharedPlugin;
    }
    
    _sharedPlugin = [[UnityPlugin alloc] init];
    
    return _sharedPlugin;
}

-(void) showHUDWithMEssage:(NSString*) msg {
    hudAddedTo = [UIApplication sharedApplication].keyWindow.rootViewController.view;
    self.hud = [MBProgressHUD showHUDAddedTo:hudAddedTo animated:YES];
    _hud.labelText = msg;
}

- (void)dismissHUD:(id)arg {
    [MBProgressHUD hideHUDForView:hudAddedTo animated:YES];
    self.hud = nil;
    hudAddedTo = nil;
}

- (void)productsLoaded:(NSNotification *)notification {
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    NSLog(@"%@", [notification object]);
    [self dismissHUD:nil];
    [[InAppPurchasePlugin sharedPlugin] purchaseProductWithIdentifier:@"com.napoleonit.car.mapscars"];
}

-(void) startedPurchasing:(NSNotification *)notification {
    [self showHUDWithMEssage:@"Покупаю..."];
}

-(void) startedRestoring:(NSNotification *)notification {
    [self showHUDWithMEssage:@"Восстанавливаю покупки..."];
}

-(void) startedSubscribing:(NSNotification *)notification {
    [self showHUDWithMEssage:@"Оформляю подписку..."];
}

-(void) productPurchased:(NSNotification *)notification {
    NSString *productPurchased = notification.object;
    if ([productPurchased isEqualToString:FreeSubscriptionProductId]) {
        if ([[InAppPurchasePlugin sharedPlugin] checkIfPurchasedProductId:FreeSubscriptionProductId]) {
        }
        _hud.labelText = @"Подписка успешно оформлена";
        _hud.customView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]] autorelease];
        _hud.mode = MBProgressHUDModeCustomView;
        [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:3.0];
    } else {
        _hud.labelText = @"Покупка успешно совершена";
        [self updateDataWithNotification:notification shouldStartDownload:YES];
    }
    UnitySendMessage("Menu and Settings", "ProductPurchased", MakeStringCopy([productPurchased UTF8String]));
}

-(void) productRestored:(NSNotification *)notification {
    _hud.labelText = @"Покупки успешно восстановлены";
    [self updateDataWithNotification:notification shouldStartDownload:NO];
    UnitySendMessage("Menu and Settings", "ProductRestored", MakeStringCopy([notification.object UTF8String]));
}

-(void) updateDataWithNotification:(NSNotification *)notification shouldStartDownload:(BOOL) shouldStart {
    _hud.customView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]] autorelease];
    _hud.mode = MBProgressHUDModeCustomView;
    [self dismissHUD:nil];
    NSString* productId = [notification object];
}

-(void) productPurchaseCancelled:(NSNotification*) notification {
    [self dismissHUD:nil];
    
    if (![[notification object] isKindOfClass:[SKPaymentTransaction class]])
        return;
    
    NSString* productId = ((SKPaymentTransaction*)[notification object]).payment.productIdentifier;
    UnitySendMessage("Menu and Settings", "ProductCancelled", MakeStringCopy([productId UTF8String]));
}

-(void) productPurchaseFailed:(NSNotification*) notification {
    if (notification.object!=nil && [notification.object isKindOfClass:[NSError class]]) {
        NSError *error = notification.object;
        _hud.detailsLabelText = error.localizedDescription;
    } else {
        _hud.detailsLabelText = @"Произошла какая-то ошибка \nво время подтверждения покупки.\nОбратитесь в техподдержку или попробуйте позднее.";
    }
    _hud.labelText = @"Ошибка!";
    _hud.customView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]] autorelease];
	_hud.mode = MBProgressHUDModeCustomView;
    [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:3.0];
    
    UnitySendMessage("Menu and Settings", "ProductFailed", "");
}

- (void)timeout:(id)arg {
    _hud.labelText = @"Ошибка!";
    _hud.detailsLabelText = @"Превышено время ожидания ответа.";
    _hud.customView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]] autorelease];
	_hud.mode = MBProgressHUDModeCustomView;
    [self performSelector:@selector(dismissHUD:) withObject:nil afterDelay:3.0];
}


-(void) checkPurchasedProduct:(NSString*)idToCheck {
    [[InAppPurchasePlugin sharedPlugin] checkIfPurchasedProductId:idToCheck];
}

@end

// When native code plugin is implemented in .mm / .cpp file, then functions
// should be surrounded with extern "C" block to conform C function naming rules
extern "C" {
    
	void _StartLookup (const char* service, const char* domain)
	{
        
	}
	
	Boolean _CheckIfPurchasedProductId (const char* prodId)
	{
        //init
        [UnityPlugin sharedPlugin];
		return [[InAppPurchasePlugin sharedPlugin] checkIfPurchasedProductId:CreateNSString(prodId)];
	}
	
	void _PurchaseProductId (const char* prodId)
	{
        //init
        [UnityPlugin sharedPlugin];
		return [[InAppPurchasePlugin sharedPlugin] purchaseProductWithIdentifier:CreateNSString(prodId)];
	}
	
	void _RestorePurchases ()
	{
        //init
        [UnityPlugin sharedPlugin];
		return [[InAppPurchasePlugin sharedPlugin] restorePurchases];
	}
}